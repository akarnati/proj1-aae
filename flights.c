/* 
 * CS61C Summer 2017
 * Name: Adarsh Karnati
 * Login: cs61c-aae
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flights.h"
#include "timeHM.h"

struct flight {
  airport_t* dst;
  int cost;
  timeHM_t dep;
  timeHM_t arr;
};

struct airport {
  char* name;
  flight_t* flights;
  int num_flights;
  int cap;
};

struct flightSys {
  airport_t* airports;
  int num_ports;
  int cap;
};

// struct timeHM {
//   int hours;
//   int minutes;
// };

/*
   This should be called if memory allocation failed.
 */
static void allocation_failed() {
    fprintf(stderr, "Out of memory.\n");
    exit(EXIT_FAILURE);
}


/*
   Creates and initializes a flight system, which stores the flight schedules of several airports.
   Returns a pointer to the system created.
 */
flightSys_t* createSystem() {
    // Allocate memory for the Flight System
    flightSys_t* sys = (flightSys_t*) malloc(sizeof(flightSys_t));
    // Check to see if allocation failed
    if (sys == NULL) {
      printf("fail create system 1");
      allocation_failed();
    }

    // Set attributes of flight system
    sys->num_ports = 0;
    sys->cap = 10;

    // Allocate memory for airports array
    sys->airports = (airport_t*) malloc(sizeof(airport_t)*(sys->cap));
    // Check to see if allocation failed
    if (sys->airports == NULL) {
      printf("fail create system 2");
      free(sys);
      allocation_failed();
    }
    return sys;
}


/* Given a destination airport, a departure and arrival time, and a cost, return a pointer to new flight_t. Note that this pointer must be available to use even
   after this function returns. (What does this mean in terms of how this pointer should be instantiated)?
*/

flight_t* createFlight(airport_t* dest, timeHM_t dep, timeHM_t arr, int c) {
  if (dest == NULL) {
    return NULL;
  }

  flight_t* new_f = (flight_t*) malloc(sizeof(flight_t));
  if (new_f == NULL) {
    printf("fail create flight 1");
    allocation_failed();
  }

  new_f->dst = dest;

  (new_f->dep).hours = dep.hours;
  (new_f->dep).minutes = dep.minutes;
  (new_f->arr).hours = arr.hours;
  (new_f->arr).minutes = arr.minutes;
  new_f->cost = c;

  return new_f;
}

/*
   Frees all memory associated with this system; that's all memory you dynamically allocated in your code.
 */
void deleteSystem(flightSys_t* s) {
    if (s == NULL) {
      return;
    }
    for (int i = 0; i < s->num_ports; i++) {
      // Freeing allocated attributes of airport i and then airport i
      free(((s->airports)[i]).flights);
      free(((s->airports)[i]).name);
    }
    // Freeing airports array
    free(s->airports);
    // Freeing flightSys s
    free(s);
}


/*
   Adds a airport with the given name to the system. You must copy the string and store it.
   Do not store "name" (the pointer) as the contents it point to may change.
 */
void addAirport(flightSys_t* s, char* name) {
    if (s == NULL || name == NULL) {
      return;
    }
    // If the array of airports is full
    if (s->num_ports == s->cap) {
      // Reallocate space that is twice the cap of the flightSys
      s->airports = (airport_t*) realloc(s->airports, 2*(s->cap)*sizeof(airport_t));
      // Check to see if allocation failed
      if (s->airports == NULL) {
        printf("fail add airport 1");
        free(s->airports);
        free(s);
        allocation_failed();
      }
      // Double cap of airport array
      s->cap = 2*(s->cap);
    }
    
    // Allocate space for the name of the airport
    (s->airports)[s->num_ports].name = malloc(strlen(name) + 1);
    // Check to see if allocation failed
    if ((s->airports)[s->num_ports].name == NULL) {
      printf("fail add airport 3");
      free(s->airports);
      free(s);
      allocation_failed();
    }
    // Copy the name of the airport into allocated space
    strcpy((s->airports)[s->num_ports].name, name);
    
    // Set the cap of the new airport
    (s->airports)[s->num_ports].cap = 10;
    // Set the flight num of the new airport
    (s->airports)[s->num_ports].num_flights = 0;
    // Allocate memory for the flights array of the new airport
    (s->airports)[s->num_ports].flights = (flight_t*) malloc(10*(sizeof(flight_t)));
    // Check to see if allocation failed
    if ((s->airports)[s->num_ports].flights == NULL) {
      printf("fail add airport 4");
      free((s->airports)[s->num_ports].name);
      free(s->airports);
      free(s);
      allocation_failed();
    }

    // Increment the system's airport counter
    s->num_ports += 1;
}


/*
   Returns a pointer to the airport with the given name.
   If the airport doesn't exist, return NULL.
 */
airport_t* getAirport(flightSys_t* s, char* name) {
    if (s == NULL || name == NULL) {
      return NULL;
    }
    airport_t* ports = s->airports;
    int count = 0;
    while(count < s->num_ports && strcmp(ports->name, name) != 0) {
      ports += 1;
      count += 1;
    }
    if (count == s->num_ports) {
      return NULL;
    }
    return ports;
}


/*
   Print each airport name in the order they were added through addAirport, one on each line.
   Make sure to end with a new line. You should compare your output with the correct output
   in flights.out to make sure your formatting is correct.
 */
void printAirports(flightSys_t* s) {
    if (s == NULL) {
      printf("Error: Flight System is NULL");
      return;
    }
    for (int i = 0; i < s->num_ports; i++) {
      printf("%s\n",((s->airports)[i]).name);
    }
}


/*
   Adds a flight to src's schedule, stating a flight will leave to dst at departure time and arrive at arrival time.
 */
void addFlight(airport_t* src, airport_t* dst, timeHM_t* departure, timeHM_t* arrival, int cost) {
    if (src == NULL || dst == NULL || departure == NULL || arrival == NULL) {
      return;
    }
    if (src->num_flights == src->cap) {
      src->flights = (flight_t*) realloc(src->flights, 2*(src->cap)*(sizeof(flight_t)));
      if (src->flights == NULL) {
        printf("fail add flight 1");
        free(src->flights);
        free(src->name);
        free(src);
        allocation_failed();
      }
      src->cap = (src->cap)*2;
    }

    (src->flights)[src->num_flights].dst = dst;
    (src->flights)[src->num_flights].dep = *departure;
    (src->flights)[src->num_flights].arr = *arrival;
    (src->flights)[src->num_flights].cost = cost;

    src->num_flights += 1;
}


/*
   Prints the schedule of flights of the given airport.

   Prints the airport name on the first line, then prints a schedule entry on each 
   line that follows, with the format: "destination_name departure_time arrival_time $cost_of_flight".

   You should use printTime (look in timeHM.h) to print times, and the order should be the same as 
   the order they were added in through addFlight. Make sure to end with a new line.
   You should compare your output with the correct output in flights.out to make sure your formatting is correct.
 */
void printSchedule(airport_t* s) {
    if (s == NULL) {
      return;
    }
    printf("%s \n", s->name);
    for (int i = 0; i < s->num_flights; i++) {
      printf("%s ", (((s->flights)[i]).dst)->name);
      printTime(&(s->flights[i].dep));
      printf(" ");
      printTime(&(s->flights[i].arr));
      printf(" $%d\n", ((s->flights)[i]).cost);
    }
}


/*
   Given a src and dst airport, and the time now, finds the next flight to take based on the following rules:
   1) Finds the cheapest flight from src to dst that departs after now.
   2) If there are multiple cheapest flights, take the one that arrives the earliest.

   If a flight is found, you should store the flight's departure time, arrival time, and cost in departure, arrival, 
   and cost params and return true. Otherwise, return false. 

   Please use the function isAfter() from time.h when comparing two timeHM_t objects.
 */
bool getNextFlight(airport_t* src, airport_t* dst, timeHM_t* now, timeHM_t* departure, timeHM_t* arrival, int* cost) {
    if (src == NULL || dst == NULL || now == NULL || departure == NULL || arrival == NULL || cost == NULL) {
      return 0;
    }
    int index = -1;
    int cur_cost = -1;
    timeHM_t past_best_time = {-1, -1};
    for (int i = 0; i < src->num_flights; i++) {
      if (strcmp(src->flights[i].dst->name, dst->name) == 0) {
        if (isAfter(&(src->flights[i].dep), now)) {
          if (src->flights[i].cost < cur_cost || cur_cost == -1) {
            index = i;
            cur_cost = src->flights[i].cost;
            past_best_time = src->flights[i].arr;
          }
          if (src->flights[i].cost == cur_cost && isAfter(&past_best_time, &src->flights[i].arr)) {
            index = i;
            cur_cost = src->flights[i].cost;
            past_best_time = src->flights[i].arr;
          }
        }
      }
    }
    if (index != -1) {
      *departure = (src->flights[index].dep);
      *arrival = (src->flights[index].arr);
      *cost = (src->flights[index].cost);
      return 1;
    } else {
      return 0;
    }
}

/* Given a list of flight_t pointers (flight_list) and a list of destination airport names (airport_name_list), first confirm that it is indeed possible to take these sequences of flights,
   (i.e. be sure that the i+1th flight departs after or at the same time as the ith flight arrives) (HINT: use the isAfter and isEqual functions).
   Then confirm that the list of destination airport names match the actual destination airport names of the provided flight_t struct's.
   sz tells you the number of flights and destination airport names to consider. Be sure to extensively test for errors (i.e. if you encounter NULL's for any values that you might expect to
   be non-NULL, return -1).

   Return from this function the total cost of taking these sequence of flights. If it is impossible to take these sequence of flights, if the list of destination airport names
   doesn't match the actual destination airport names provided in the flight_t struct's, or if you run into any errors mentioned previously or any other errors, return -1.
*/
int validateFlightPath(flight_t** flight_list, char** airport_name_list, int sz) {
    if (flight_list == NULL || airport_name_list == NULL || sz <= 0) {
      return -1;
    }
    int tot_cost = 0;
    for (int i = 0; i < sz; i++) {
      if (flight_list[i] == NULL || (airport_name_list)[i] == NULL) {
        return -1;
      }
      if (strcmp((flight_list)[i]->dst->name, (airport_name_list)[i]) != 0) {
        return -1;
      }
      if (i < sz-1) {
        if (!isAfter(&(flight_list)[i+1]->dep, &(flight_list)[i]->arr) 
            && !isEqual(&(flight_list)[i+1]->dep, &(flight_list)[i]->arr)) {
          return -1;
        }
      }
      tot_cost += (flight_list)[i]->cost;
    }
    return tot_cost;
}


